const path = require('path');
const webpack = require('webpack');

module.exports = {
	context: path.resolve(__dirname, "src"),
    entry: {
        index: './index.jsx',
        vendor: ["axios", "react", "react-autobind", "react-dom", "react-bootstrap", "react-highlight", "js-storage"]
    },
    output: {
        path: path.join(__dirname, './dist'),
        filename: '[name].js'
    },
    module: {
        rules: [{
            test: /\.jsx?$/,
            exclude: /(node_modules|bower_components|public)/,
            use: "babel-loader"
        }, {
            test: /\.json$/,
            use: 'json-loader'
        }, {
	        test: /\.svg$/,
	        use: 'svg-loader?pngScale=2'
        }, {
			test: /\.css$/,
			use: ['style-loader', 'css-loader']
            }
        ]
    },
    resolve: {
        modules: [path.resolve(__dirname, "src"), 'node_modules'],
        extensions: ['.jsx', '.json', '.css', '.js'],
	    alias: {
		    configPath: path.resolve(__dirname, 'src/app/config/'),
		    utilsPath: path.resolve(__dirname, 'src/app/utils/'),
		    componentsPath: path.resolve(__dirname, 'src/app/components/'),
	    }
    },
	plugins: [
		//new webpack.optimize.UglifyJsPlugin(),
		new webpack.optimize.CommonsChunkPlugin({
			name: ['common', 'vendor'],
			minChunks: 2
		}),
		new webpack.DefinePlugin({
            //define constants here, will be replaced in all text to its value
		}),
		new webpack.ProvidePlugin({
			//set libraries autoload
		})
	],
	devtool: 'source-map'
};