export const userLoginSnippet = `POST /user/authenticate/ HTTP/1.1
Content-Type: application/json; charset=utf-8

{
  "login": "johnny_bravo",
  "password": "myAwsomePassword123"
}

SUCCESS (200 OK)
{
  "user_id": 12,
  "token": "ve5186fhudi1gjhl61f5d236o46luu92",
  "status": "success"
}

FAIL (400 Bad Request)
{
  "error": [
    "wrong_login_password"
  ],
  "error_description": [
    "Incorrect login or password"
  ],
  "status": "fail"
}`;

export const userRegisterSnippet = `POST /user/register/ HTTP/1.1
Content-Type: application/json; charset=utf-8

{
  "login": "rick_sanchez",
  "password": "Yeah_science_81tch",
  "name": "Jessy Pinkman"
}

SUCCESS (200 OK)
{
  "user_id": "15",
  "token": "ve5186fhudi1gjhl61f5d236o46luu92",
  "status": "success"
}

FAIL (400 Bad Request)
{
  "error": [
    "short_password"
  ],
  "error_description": [
    "Password is too short"
  ],
  "status": "fail"
}
...
{
  "error": [
    "user_exists"
  ],
  "error_description": [
    "User \`jessy1\` already registered"
  ],
  "status": "fail"
}
`;

export const userLogoutSnippet = `POST /user/logout/ HTTP/1.1
Content-Type: application/json; charset=utf-8

{
  "token": "ve5186fhudi1gjhl61f5d236o46luu92"
}

SUCCESS (200 OK)
{
  "status": "success"
}

FAIL (400 Bad Request)
{
  "error": [
    "access_denied"
  ],
  "error_description": [
    "Token expired or invalid"
  ],
  "status": "fail"
}`;

export const recipeCreateSnippet = `POST /recipe/create/ HTTP/1.1
Content-Type: application/json; charset=utf-8

{
  "token": "ve5186fhudi1gjhl61f5d236o46luu92",
  "title": "My awsome recipe title",
  "description": "Take mashed potatoes and chops. Enjoy"
}

SUCCESS (200 OK)
{
  "recipe_id": "8",
  "status": "success"
}

FAIL (400 Bad Request)
{
  "error": [
    "validation_error"
  ],
  "error_description": [
    "One or more required fields are missing values"
  ],
  "status": "fail"
}
`;

export const recipeUpdateSnippet = `POST /recipe/update/ HTTP/1.1
Content-Type: application/json; charset=utf-8

{
  "token": "ve5186fhudi1gjhl61f5d236o46luu92",
  "title": "My awsome vegan recipe",
  "description": "Take mashed potatoes and carrot cakes. Enjoy"
  "recipe_id": 10
}

SUCCESS (200 OK)
{
  "status": "success"
}

FAIL (400 Bad Request)
{
  "error": [
    "recipe_does_not_exits"
  ],
  "error_description": [
    "There is no recipe with id 11"
  ],
  "status": "fail"
}
...
{
  "error": [
    "validation_error"
  ],
  "error_description": [
    "One or more required fields are missing values"
  ],
  "status": "fail"
}
`;

export const recipeDeleteSnippet = `POST /recipe/delete/ HTTP/1.1
Content-Type: application/json; charset=utf-8

{
  "token": "ve5186fhudi1gjhl61f5d236o46luu92",
  "title": "My awsome vegan recipe",
  "description": "Take mashed potatoes and carrot cakes. Enjoy"
  "recipe_id": 10
}

SUCCESS (200 OK)
{
  "status": "success"
}

FAIL (400 Bad Request)
{
  "error": [
    "recipe_does_not_exits"
  ],
  "error_description": [
    "There is no recipe with id 11"
  ],
  "status": "fail"
}
...
{
  "error": [
    "validation_error"
  ],
  "error_description": [
    "One or more required fields are missing values"
  ],
  "status": "fail"
}
`;

export const imageUploadSnippet = `POST /image/upload/ HTTP/1.1
Content-Type: application/json; charset=utf-8

{
  "token": "ve5186fhudi1gjhl61f5d236o46luu92",
  "recipe_id": 10,
  "image_name": "cool_photo.jpg",
  "image_data": "...some base64 encoded data" // whithout 'data:*/*;base64,'
}

SUCCESS (200 OK)
{
  "status": "success"
}

FAIL (400 Bad Request)
{
  "error": [
    "recipe_does_not_exits"
  ],
  "error_description": [
    "There is no recipe with id 11"
  ],
  "status": "fail"
}
...
{
  "error": [
    "no_images_were_uploaded"
  ],
  "error_description": [
    "None of images were uploaded successuflly"
  ],
  "status": "fail"
}
...
{
  "error": [
    "validation_error"
  ],
  "error_description": [
    "One or more required fields are missing values"
  ],
  "status": "fail"
}
`;

export const imageDeleteSnippet = `POST /image/delete/ HTTP/1.1
Content-Type: application/json; charset=utf-8

{
  "token": "ve5186fhudi1gjhl61f5d236o46luu92",
  "image_id": 123 //single id or array of ids
}

SUCCESS (200 OK)
{
  "success": [123],
  "fail": [],
  "status": "success"
}

FAIL (400 Bad Request)
{
  "error": [
    "no_image_ids"
  ],
  "error_description": [
    "There were no correct image ids to delete"
  ],
  "status": "fail"
}
...
{
  "error": [
    "no_images_were_deleted"
  ],
  "error_description": [
    "None of images were deleted successuflly"
  ],
  "status": "fail"
}
`;

export const recipeListSnippet = `POST /recipe/list/ HTTP/1.1
Content-Type: application/json; charset=utf-8

{
  "token": "ve5186fhudi1gjhl61f5d236o46luu92"
}

SUCCESS (200 OK)
{
  "list": [{
    "id": 45,
    "user_id": 12,
    "title": "My recipe",
    "description": "My description",
    "date_added": "2018-05-02T19:27:15+00:00"
    "date_updated": "2018-05-02T21:43:22+00:00"
  },...]
  "status": "success"
}

FAIL (400 Bad Request)
{
  "error": [
    "validation_error"
  ],
  "error_description": [
    "One or more required fields are missing values"
  ],
  "status": "fail"
}
`;

