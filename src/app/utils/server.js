import {Config} from "configPath/config";
import axios from "axios";

class Server {

	static api() {
		return axios.create({
			baseURL: `${Config.apiUrl}`,
			timeout: 10000,
			transformRequest: [(data) => JSON.stringify(data)],
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
			},
			withCredentials: false
		});
	}
}

export default Server;