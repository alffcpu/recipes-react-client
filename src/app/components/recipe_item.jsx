import React from 'react';
import autoBind from 'react-autobind';
import { Row, Col, Media, Panel, Button, Carousel } from 'react-bootstrap';
import {Icon} from "componentsPath/icon";
import moment from 'moment';

class RecipeItem extends React.Component {

	constructor(props) {
		super(props);
		autoBind(this);
	}

	render() {
		const {id,
			title,
			description,
			images,
			index,
			date_added: added,
			date_updated: updated,
			edit: openEdit,
			remove: openRemove,
			helpEdit,
			helpRemove
		} = this.props;

		return (
			<Row>
				<Col md={11}>
					<Panel>
						<Panel.Body>
							<Media>
								{images && images.length === 1 ?
									<Media.Left>
										<img className="item-thb" src={images[0].path} alt={title} />
									</Media.Left>
									: null
								}
								{images && images.length > 1 ?
									<Media.Left>
										<Carousel indicators={false} prevIcon={<Icon name="angle-left" />} nextIcon={<Icon name="angle-right" />}>
											{ images.map((image) => {
												return <Carousel.Item key={`slide-${id}-${image.id}`}>
													<img className="item-thb" alt={title} src={image.path} />
												</Carousel.Item>
												})
											}
										</Carousel>
									</Media.Left>
									: null
								}
								<Media.Body>
									<Media.Heading>{title}</Media.Heading>
									{description.split("\n").map((value,index) => <p key={`p${id}-${index}`}>{value}</p>)}
									<p className="item-date">
										{ updated ?
											<span><span className="text-success">{ moment(updated).format('DD.MM.YYYY h:mm') }</span> / </span>
											:
											null
										}
										<span className="text-warning">{ moment(added).format('DD.MM.YYYY h:mm') }</span>
									</p>
								</Media.Body>
							</Media>
						</Panel.Body>
					</Panel>
				</Col>
				<Col md={1} className="item-actions">
					<div className="relative">
						<Button type="button"
						        onClick={ (event) => {openEdit(id, event)} }
						        className="btn btn-sm"
						><Icon name="edit"/></Button>
						{index === 0 ? <a href="#" onClick={helpEdit}><Icon name="question-circle"/></a> : null}
					</div>
					<div className="relative">
						<Button type="button"
						        onClick={ (event) => {openRemove(id, event)} }
						        className="btn btn-sm"><span className="text-danger"
						><Icon name="times"/></span></Button>
						{index === 0 ? <a href="#" onClick={helpRemove}><Icon name="question-circle"/></a> : null}
					</div>
				</Col>
			</Row>
		);
	}
}
export default RecipeItem;