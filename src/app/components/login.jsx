import React from 'react';
import autoBind from 'react-autobind';
import { FormGroup, Alert, ControlLabel, FormControl, Button } from 'react-bootstrap';
import Highlight from 'react-highlight';
import {userLoginSnippet} from 'configPath/snippets';
import Server from 'utilsPath/server';
import Storages from 'js-storage';

class Login extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			login: '',
			passowrd: '',
			errors: []
		};

		this.elements = {};

		//ref={(input) => {this.Progress[0] = input }}
		this.storagePares = {login: 'last_login', password: 'last_password'};

		autoBind(this);
	}

	handleChange(e) {
		this.setState({ [e.target.name]: e.target.value });
	}

	componentDidMount() {
		const storage = Storages.sessionStorage;
		const state = {};
		for (const keyState in this.storagePares) {
			const keyStorage = this.storagePares[keyState];
			if (storage.isSet(keyStorage) && !storage.isEmpty(keyStorage)) {
				state[keyState] = storage.get(keyStorage);
				if (this.elements[keyState]) this.elements[keyState].value = state[keyState];
			}
		}

		const autofillSize = Object.keys(state).length;
		if (autofillSize) {
			this.setState(state, () => {
				if (autofillSize === 2) {
					this.submitForm();
				}
			});
		}
	}

	submitForm() {

		const {login, password} = this.state;
		const storage = Storages.sessionStorage;
		for (const keyState in this.storagePares) {
			const keyStorage = this.storagePares[keyState];
			storage.set(keyStorage, this.state[keyState]);
		}

		this.setState({errors: []});
		this.props.setAppState({busy: true}, () => {
			const api = Server.api();
			api.post('user/authenticate/', {
				login,
				password
			}).then((response) => {
				this.props.setAppState({busy: false, route: 'recipes', token: response.data.token});
			}).catch((response) => {
				console.log( response );
				if (response && response.response) {
					this.props.setAppState({busy: false}, () => {
						this.setState({errors: response.response.data.error_description || ['server error']});
					});
				} else {
					this.props.setAppState({busy: false}, () => {
						this.setState({errors: ['Unable to connect to server!']});
					});
				}
			});
		})
	}

	render() {
		return (
		<div>
			{ this.state.errors.length ?
				<Alert bsStyle="danger">
					{ this.state.errors.map((value, index) => <p key={`error${index}`}>{value}</p>) }
				</Alert>
				: null
			}
			<form className="spaced-bottom login-form">
				<FormGroup>
					<ControlLabel>Login</ControlLabel>
					<FormControl
						type="text"
						name="login"
						onChange={this.handleChange}
						inputRef={(input) => {this.elements['login'] = input }}
					/>
				</FormGroup>
				<FormGroup>
					<ControlLabel>Password</ControlLabel>
					<FormControl
						type="password"
						name="password"
						onChange={this.handleChange}
						inputRef={(input) => {this.elements['password'] = input }}
					/>
				</FormGroup>
				<FormGroup>
					<Button type="button" className="btn-primary" onClick={this.submitForm}>Sign In</Button>
				</FormGroup>
			</form>
			<Highlight className="http">{userLoginSnippet}</Highlight>
		</div>
		);
	}
}
export default Login;