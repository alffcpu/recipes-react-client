import React from 'react';
import autoBind from 'react-autobind';
import { Modal, Button } from 'react-bootstrap';
import Highlight from "react-highlight";

class ApiModal extends React.Component {

	constructor(props) {
		super(props);
		autoBind(this);
	}

	handleClose() {
		if (this.props.callback) this.props.callback();
	}

	render() {
		const {title, content} = this.props;
		return (
			<Modal show={this.props.show} onHide={this.handleClose}>
				<Modal.Header closeButton>
					<Modal.Title>{title || `API info`}</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Highlight className="http">{content}</Highlight>
				</Modal.Body>
				<Modal.Footer>
					<Button onClick={this.handleClose}>Close</Button>
				</Modal.Footer>
			</Modal>
		);
	}
}
export default ApiModal;