import autoBind from "react-autobind";
import React from "react";
import { Collapse, Grid, Row, Col, Thumbnail, Button, FormGroup, HelpBlock, FormControl, Clearfix } from 'react-bootstrap';
import array_chunk from 'locutus/php/array/array_chunk';
import {imageUploadSnippet, imageDeleteSnippet} from "configPath/snippets";
import Highlight from "react-highlight";
import {Icon} from "componentsPath/icon";

class Images extends React.Component {

	constructor(props, context) {
		super(props, context);
		this.state = {
			helpUpload: false,
			helpDelete: false
		};
		this.imageUpload = null;
		autoBind(this);
	}

	componentDidUpdate() {
		if(this.imageUpload) {
			this.imageUpload.setAttribute('accept', 'image/x-png,image/gif,image/jpeg');
		}
	}

	toggleHelpUpload() {
		this.setState({helpUpload: !this.state.helpUpload, helpDelete: false});
	}

	toggleHelpRemove() {
		this.setState({helpDelete: !this.state.helpDelete, helpUpload: false});
	}

	render() {
		const { images, upload, remove, itemId, fileSelect } = this.props;
		const chunked = array_chunk(images || [], 3);

		return <Grid className="item-edit-thumbnails" fluid>
			{ chunked.map((images, index) => {
				return <Row key={`image-row-${index}`}>
					{images.map((image, key) => {
						return <Col md={4} key={`image-row-${key}`}>
							<Thumbnail src={image.path} alt={image.name}>
								<div>
									<p>{image.name}</p>
									<Clearfix />
									<Button bsStyle="danger" bsSize="xsmall" onClick={ () => { remove(image.id)} }>Remove</Button>
									{ !index && !key ? <a href="#" onClick={this.toggleHelpRemove}><Icon name="question-circle"/></a> : null}
								</div>
							</Thumbnail>
						</Col>
						}
					)}
				</Row>
			}) }
			<Row>
				<Col md={12}>
					<FormGroup>
						<FormControl
							type="file"
							id="image-upload"
							className="pull-left"
							inputRef={ (input) => { this.imageUpload = input } }
							onChange={ (event) => { fileSelect(event.target) } }
						/>
						{ itemId ? <Button className="pull-left" bsStyle="success" bsSize="small" onClick={ () => { upload( this.imageUpload )} }>Upload image</Button> : null }
						{ itemId ? <a href="#" onClick={this.toggleHelpUpload}><Icon name="question-circle"/></a> : null }
						<Clearfix />
						<HelpBlock>jpg, png, gif are supported</HelpBlock>
					</FormGroup>
				</Col>
			</Row>
			<Collapse in={this.state.helpUpload}>
				<Row>
					<Col md={12}>
						<Highlight className="http">{imageUploadSnippet}</Highlight>
					</Col>
				</Row>
			</Collapse>
			<Collapse in={this.state.helpDelete}>
				<Row>
					<Col md={12}>
						<Highlight className="http">{imageDeleteSnippet}</Highlight>
					</Col>
				</Row>
			</Collapse>
		</Grid>
	}
}

export default Images;