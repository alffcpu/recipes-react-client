import React from 'react';
import { Navbar, Nav, NavItem, Grid, Col, Row, Button, Modal } from 'react-bootstrap';
import {Config} from 'configPath/config';
import {Icon} from "componentsPath/icon";
import autoBind from 'react-autobind';
import {userLogoutSnippet} from "configPath/snippets";
import ApiModal from "componentsPath/api_modal";

class Header extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			modal: false
		};

		autoBind(this);
	}

	render() {
		const {user, setAppState, route} = this.props;

		return <Navbar>
			<Grid>
				<Row>
					<Col md={8} mdOffset={2}>
						<Navbar.Header>
							<Navbar.Brand>
								{Config.appName}
							</Navbar.Brand>
							<Navbar.Toggle />
						</Navbar.Header>
						<Navbar.Collapse>
							{ user ?
							<Navbar.Text pullRight className="signed-in">
								<Icon name="user" /><span>Signed in as: {user.name}</span>
								<a href="#" onClick={this.props.signOut} className="btn btn-link btn-xs spaced-left">Sign out</a>
								<a href="#" className="btn btn-link btn-xs" onClick={ (e)=>{ e.preventDefault(); this.setState({modal: true}) }}><Icon name="question-circle"/></a>
							</Navbar.Text>
							:
							<Nav pullRight>
								<NavItem active={route === 'login'} eventKey={-1} href="#" onClick={ () => {setAppState({route: 'login'})} }>
									Sign in
								</NavItem>
								<NavItem active={route === 'register'} eventKey={-1} href="#" onClick={ () => {setAppState({route: 'register'})} }>
									Register
								</NavItem>
							</Nav>
							}
						</Navbar.Collapse>
					</Col>
				</Row>
			</Grid>
			<ApiModal show={this.state.modal} content={userLogoutSnippet} callback={ () => {this.setState({modal: false})} } />
		</Navbar>;
	}
}
export default Header;