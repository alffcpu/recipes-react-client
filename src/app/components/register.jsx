import React from 'react';
import autoBind from 'react-autobind';
import { FormGroup, Alert, ControlLabel, FormControl, Button } from 'react-bootstrap';
import Highlight from 'react-highlight';
import {userRegisterSnippet} from 'configPath/snippets';
import Server from "utilsPath/server";

class Register extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			login: '',
			passowrd: '',
			repeated: '',
			name: '',
			errors: []
		};
		autoBind(this);
	}

	handleChange(e) {
		this.setState({ [e.target.name]: e.target.value });
	}

	submitForm() {
		const {login, password, repeated, name} = this.state;

		if (password !== '' && password !== repeated) {
			this.setState({errors: ["Password and repeated password are not equal"]});
			return;
		}

		this.setState({errors: []});
		this.props.setAppState({busy: true}, () => {
			const api = Server.api();
			api.post('user/register/', {
				login,
				password,
				name
			}).then((response) => {
				this.props.setAppState({busy: false, route: 'recipes', token: response.data.token});
			}).catch((response) => {
				this.props.setAppState({busy: false}, () => {
					this.setState({errors: response.response.data.error_description || ['server error']});
				});
			});
		})
	}

	render() {
		return (
		<div>
			{ this.state.errors.length ?
				<Alert bsStyle="danger">
					{ this.state.errors.map((value, index) => <p key={`error${index}`}>{value}</p>) }
				</Alert>
				: null
			}
			<form className="spaced-bottom">
				<FormGroup>
					<ControlLabel>Name</ControlLabel>
					<FormControl
						type="text"
						name="name"
						onChange={this.handleChange}
					/>
				</FormGroup>
				<FormGroup>
					<ControlLabel>Login</ControlLabel>
					<FormControl
						type="text"
						name="login"
						onChange={this.handleChange}
					/>
				</FormGroup>
				<FormGroup>
					<ControlLabel>Password</ControlLabel>
					<FormControl
						type="password"
						name="password"
						onChange={this.handleChange}
					/>
				</FormGroup>
				<FormGroup>
					<ControlLabel>Repeat password</ControlLabel>
					<FormControl
						type="password"
						name="repeated"
						onChange={this.handleChange}
					/>
				</FormGroup>
				<FormGroup>
					<Button type="button" className="btn-primary" onClick={this.submitForm}>Register</Button>
				</FormGroup>
			</form>
			<Highlight className="http">
			{userRegisterSnippet}
	</Highlight>
		</div>
		);
	}
}
export default Register;