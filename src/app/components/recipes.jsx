import React from 'react';
import autoBind from 'react-autobind';
import { Grid, Button } from 'react-bootstrap';
import {recipeCreateSnippet, recipeUpdateSnippet, recipeDeleteSnippet, recipeListSnippet} from 'configPath/snippets';
import Server from 'utilsPath/server';
import {Icon} from "componentsPath/icon";
import RecipeItem from "componentsPath/recipe_item";
import ApiModal from "componentsPath/api_modal";
import EditRecipe from "componentsPath/edit_recipe";
import ConfirmRemoveModal from "componentsPath/remove_confirm";
import Highlight from "react-highlight";

class Recipes extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			list: [],
			modalAdd: false,
			modalEdit: false,
			modalRemove: false,
			recipeEdit: false,
			recipeEditId: 0,
			confirmRemove: false,
			itemToRemove: null
		};
		autoBind(this);
	}

	reloadList() {
		const api = Server.api();
		api.post('recipe/list/', {
			token: this.props.token
		}).then((response) => {
			this.setState({busy: false, list: response.data.list || []});
		}).catch((error) => {
			this.setState({
				busy: false,
				error: error.response.data.error_description.join(', '),
				errorCallback: () => {
					this.componentDidMount();
				}
			});
		});
	}

	componentDidMount() {
		this.reloadList();
	}

	openModalAdd(e) {
		e.preventDefault();
		this.setState({modalAdd: true});
	}

	closeModalAdd() {
		this.setState({modalAdd: false})
	}

	openModalEdit(e) {
		e.preventDefault();
		this.setState({modalEdit: true});
	}

	closeModalEdit() {
		this.setState({modalEdit: false})
	}

	openModalRemove(e) {
		e.preventDefault();
		this.setState({modalRemove: true});
	}

	closeModalDelete() {
		this.setState({modalRemove: false})
	}

	openEditRecipe(id, event) {
		event.target.blur();
		this.setState({
			recipeEdit: true,
			recipeEditId: id
		});
	}

	editOnCancel() {
		this.setState({recipeEdit: false}, () => {
			this.reloadList();
		});
	}

	editOnUpdate() {
		this.setState({recipeEdit: false}, () => {
			this.reloadList();
		});
	}

	removeRecipe(item) {
		this.setState({
			confirmRemove: true,
			itemToRemove: item
		});
	}

	removeRecipeConfirmed() {
		this.setState({busy: true});
		const api = Server.api();
		api.post('recipe/delete/', {
			token: this.props.token,
			recipe_id: this.state.itemToRemove.id
		}).then((response) => {
			this.setState({
					confirmRemove: false,
					itemToRemove: null
				}, () => {
					this.reloadList();
			});
		}).catch((error) => {
			this.setState({
				busy: false,
				error: error.response.data.error_description.join(', ')
			});
		});
	}

	removeRecipeCancelled() {
		this.setState({
			confirmRemove: false,
			itemToRemove: null
		});
	}

	render() {
		const {list, recipeEdit, modalAdd, recipeEditId, itemToRemove, confirmRemove} = this.state;
		const {user, token} = this.props;
		if (!user && !list.length) {
			return <div className="text-center"><Icon name="spinner fa-pulse fa-4x"/></div>
		} else {
			return [
				<p key="list-panel" className="text-center list-panel">
					<Button onClick={ (event) => {this.openEditRecipe(0, event)} } bsStyle="primary">Add recipe</Button>
					<a href="#" onClick={this.openModalAdd}><Icon name="question-circle"/></a>
					<ApiModal show={modalAdd} content={recipeCreateSnippet} callback={this.closeModalAdd} />
				</p>,
				<Grid key="list-list" fluid className="list-list">
					{list ? list.map((item, index) => <RecipeItem
						edit={this.openEditRecipe}
						remove={ (id, event) => { event.target.blur(); this.removeRecipe(item)} }
						index={index}
						key={`item-${item.id}`}
						helpEdit={this.openModalEdit}
						helpRemove={this.openModalRemove}
						{...item} />
					) : null}
					{list && list.length ? <Highlight className="http">{recipeListSnippet}</Highlight> : null}
					<ApiModal show={this.state.modalEdit} content={recipeUpdateSnippet} callback={ this.closeModalEdit } />
					<ApiModal show={this.state.modalRemove} content={recipeDeleteSnippet} callback={ this.closeModalDelete } />
					<ConfirmRemoveModal
						show={confirmRemove}
						item={itemToRemove}
						accept={ this.removeRecipeConfirmed }
						decline= { this.removeRecipeCancelled }
					/>
				</Grid>,
				<EditRecipe key="list-modal" token={token} onCancel={this.editOnCancel} onUpdate={this.editOnUpdate} show={recipeEdit} id={recipeEditId} />,
			]
		}
	}
}
export default Recipes;