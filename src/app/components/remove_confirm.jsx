import React from 'react';
import { Modal, Button } from 'react-bootstrap';

class ConfirmRemoveModal extends React.Component {
	render() {
		const {item, accept, decline, show} = this.props;
		return (
			<Modal show={show} onHide={decline}>
				<Modal.Body>
					{item ?
						<h4 className="no-margin text-danger">Remove recipe <em>"{item.title}"</em>. Are
							you sure?</h4>
						:
						<h4 className="no-margin">&nbsp;</h4>
					}
				</Modal.Body>
				<Modal.Footer>
					<Button onClick={accept} bsStyle="danger">Remove</Button>
					<Button onClick={decline}>Cancel</Button>
				</Modal.Footer>
			</Modal>
		);
	}
}
export default ConfirmRemoveModal;