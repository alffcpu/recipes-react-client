import autoBind from "react-autobind";
import React from "react";
import { Modal, Button, FormGroup, ControlLabel, FormControl, Alert } from 'react-bootstrap';
import {Icon} from "componentsPath/icon";
import Server from "utilsPath/server";
import Images from "componentsPath/images";

class EditRecipe extends React.Component {

	constructor(props, context) {
		super(props, context);
		this.state = {
			ready: false,
			error: false,
			images: [],
			errorImages: ''
		};
		this.elements = {};
		this.inputFileUpload = null;
		autoBind(this);
	}

	handleHide() {
		this.setState({id: 0}, () => {
			if (this.props.onCancel) this.props.onCancel();
		});
	}

	handleSave() {
		if (this.props.id) {
			this.handleSaveUpdate();
		} else {
			this.handleSaveCreate();
		}
	}

	getValues() {
		const result = {};
		for (const key in this.elements) {
			const element = this.elements[key];
			result[key] = element.value;
		}
		return result;
	}

	handleSaveUpdate() {

		const { id, token } = this.props;
		const { title, description } = this.getValues();

		const api = Server.api();
		api.post('recipe/update/',
			{
				token,
				title,
				description,
				recipe_id: id
			}
		).then((response) => {
			this.handleSaveComplete();
		}).catch((error) => {
			this.setImagesError(error);
		});
	}

	handleSaveCreate() {
		const { id, token } = this.props;
		const { title, description } = this.getValues();

		const api = Server.api();
		api.post('recipe/create/',
			{
				token,
				title,
				description
			}
		).then((response) => {

			if (this.inputFileUpload && this.inputFileUpload.value !== "") {
				const {recipe_id} = response.data;
				const reader = new FileReader();
				const { token } = this.props;
				const file = this.inputFileUpload.files[0];

				reader.addEventListener("load", (event) => {
					const data = reader.result.split(',');
					const api = Server.api();
					api.post('image/upload/', {
						token,
						recipe_id: recipe_id,
						image_name: file.name,
						image_data: data[1]
					}).then((response) => {
						this.handleSaveComplete();
					}).catch((error) => {
						this.setImagesError(error);
					});
				}, false);
				reader.readAsDataURL(file);
			} else {
				this.handleSaveComplete();
			}
		}).catch((error) => {
			this.setImagesError(error);
		});
	}

	handleSaveComplete() {
		if (this.inputFileUpload) {
			this.inputFileUpload.value = '';
		}
		if (this.props.onUpdate) {
			this.props.onUpdate(this.getValues(), () => {
				this.handleHide();
			});
		} else {
			this.handleHide();
		}
	}

	imageSelected(inputRef) {
		this.inputFileUpload = inputRef;
	}

	componentWillReceiveProps(nextProps) {
		const {id} = nextProps;
		if (!id) {
			this.setState({
				ready: true,
				error: false,
				images: [],
				errorImages: ''
			});
		} else {
			const api = Server.api();
			api.post('recipe/item/', {
				token: this.props.token,
				recipe_id: id
			}).then((response) => {
				const {images} = response.data.item;
				this.setState({
					ready: true,
					error: false,
					images: images || [],
					errorImages: ''
				});

				for (const key in this.elements) {
					const element = this.elements[key];
					if (response.data.item[element.name]) {
						element.value = response.data.item[element.name];
					}
				}

			}).catch((error) => {
				if (error.response) {
					this.setState({
						ready: false,
						error: error.response.data.error_description.join(', ')
					});
				}
			});
		}
	}

	imageUpload(inputRef) {
		const file = inputRef.files[0];
		const reader = new FileReader();
		const { token, id } = this.props;
		reader.addEventListener("load", (event) => {
			const data = reader.result.split(',');
			const api = Server.api();
			api.post('image/upload/', {
				token,
				recipe_id: id,
				image_name: file.name,
				image_data: data[1]
			}).then((response) => {
				return api.post('recipe/item/', {
					token,
					recipe_id: id
				});
			}).then((response) => {
				const {images} = response.data.item;
				this.setState({
					images: images || [],
					errorImages: ''
				});
				inputRef.value = null;
			}).catch((error) => {
				console.log(error);
				this.setImagesError(error);
				inputRef.value = null;
			});
		}, false);
		if (file) {
			reader.readAsDataURL(file);
		}
	}

	setImagesError(error) {
		console.log( error );
		let errorText = 'Uploading error';
		if (error.response && error.response.data && error.response.data.error_description) {
			errorText = error.response.data.error_description.join(',');
		}
		this.setState({
			errorImages: errorText
		});
	}

	imageDelete(image_id) {
		const { token, id } = this.props;
		const api = Server.api();
		api.post('image/delete/', {
			token,
			image_id: image_id
		}).then((response) => {
			return api.post('recipe/item/', {
				token,
				recipe_id: id
			});
		}).then((response) => {
			const {images} = response.data.item;
			this.setState({
				images: images || [],
				errorImages: ''
			});
		}).catch((error) => {
			this.setImagesError(error);
		});
	}

	render() {
		const { id } = this.props;
		const { ready, error, images, errorImages } = this.state;

		let content;
		if (error) {
			content = <Alert bsStyle="danger">
				<div className={'spaced-bottom'}>{error}</div>
				<Button bsStyle="danger" type="button" onClick={ () => {this.componentWillReceiveProps({id}) } }>Retry</Button>
			</Alert>;
		} else if (!ready) {
			content = <div className="text-center"><Icon name={'spinner fa-pulse fa-2x'} /></div>;
		} else {
			content = <form>
				<FormGroup controlId="edit-layer">
					<ControlLabel>Title</ControlLabel>
					<FormControl
						type="text"
						name="title"
						inputRef={(input) => {this.elements['title'] = input }}
					/>
					<ControlLabel>Description</ControlLabel>
					<FormControl
						componentClass="textarea"
						name="description"
						inputRef={ (input) => {this.elements['description'] = input } }
					/>
				</FormGroup>
				{ errorImages !== "" ? <Alert bsStyle="danger" onDismiss={ () => {this.setState({errorImages: ''})} }>{errorImages}</Alert> : null }
				<Images
					itemId={id}
					images={images}
					upload={this.imageUpload}
					remove={this.imageDelete}
					fileSelect={this.imageSelected}
				/>
			</form>;
		}

		return (
			<Modal
				show={this.props.show}
				onHide={this.handleHide}
				aria-labelledby="contained-modal-title"
				keyboard={false}
			>
				<Modal.Header closeButton>
					<Modal.Title id="contained-modal-title">
						{id ? 'Edit' : 'Add'} recipe <small>{id ? `(ID: ${id})` : null}</small>
					</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					{content}
				</Modal.Body>
				<Modal.Footer>
					<Button bsStyle="primary" onClick={this.handleSave}>{id ? 'Update' : 'Add'} recipe</Button>
					<Button onClick={this.handleHide}>Cancel</Button>
				</Modal.Footer>
			</Modal>
		);
	}
}

export default EditRecipe;