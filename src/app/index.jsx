import React from 'react';
import autoBind from 'react-autobind';
import { Grid, Col, Row, Alert, Button, Panel } from 'react-bootstrap';
import Login from 'componentsPath/login';
import Register from 'componentsPath/register';
import Header from 'componentsPath/navbar';
import {Icon} from "componentsPath/icon";
import Server from 'utilsPath/server';
import Recipes from "componentsPath/recipes";
import Storages from "js-storage";

class App extends React.Component {

	constructor(props) {
		super(props);

		this.state = App.emptyState();

		autoBind(this);
	}

	static emptyState() {
		return {
			error: '',
			errorCallback: null,
			token: '',
			user: null,
			route: 'login',
			busy: false
		};
	}

	componentDidCatch() {
		this.setState({
			error: 'Application error. See console.',
			errorCallback: null
		});
	}

	componentDidUpdate() {
		const {user, token, error} = this.state;
		if (token !== '' && !user && error === '') {
			const api = Server.api();
			api.post('/user/info/', {
				token
			}).then((response) => {
				const {id, login, last_visit: lastVisit, name, registered} = response.data;
				this.setState({
					user: {id, login, lastVisit, name, registered}
				});
			}).catch((error) => {
				this.setState({
					error: error.response.data.error_description.join(', '),
					errorCallback: () => {
						this.setState({user: null, error: '', errorCallback: null});
					}
				});
			});
		}
	}

	setAppState(stateData, callback) {
		this.setState(stateData, callback);
	}

	signOut(e) {
		e.preventDefault();
		this.setState({busy: true}, () => {
			const api = Server.api();
			api.post('user/logout/', {
				token: this.state.token
			}).then(() => {
				this.logOut();
			}).catch((response) => {
				console.log(response);
				this.logOut();
			});
		});
	}

	logOut() {
		const storage = Storages.sessionStorage;
		storage.remove('last_password');
		this.setState(App.emptyState());
	}

	render() {
		const {token, error, errorCallback, user, route, busy} = this.state;
		const routes = {
			token: {
				recipes: <Recipes user={user} setAppState={this.setAppState} token={token} />,
				recipe: ''
			},
			noToken: {
				login: <Login setAppState={this.setAppState} />,
				register: <Register setAppState={this.setAppState} />
			}
		};
		const error404 = <Panel>
			<Panel.Heading>Page not found! ({route})</Panel.Heading>
			<Panel.Body>Page you are looking for is not exists! Please, start from the beginning</Panel.Body>
		</Panel>;

		let content;
		if (error !== '') {
			content = <Alert bsStyle="danger">
				<div className={'spaced-bottom'}>{error}</div>
				{errorCallback ?  <Button bsStyle="danger" type="button" onClick={ () => {errorCallback()} }>Retry</Button> : null}
			</Alert>
		} else {
			content = routes[token === "" ? "noToken" : "token"][route] ? routes[token === "" ? "noToken" : "token"][route] : error404;
		}

		return <div className={'app-wrapper'}>
			<Header user={user} token={token} setAppState={this.setAppState} signOut={this.signOut} route={route} />;
			<Grid>
				<Row>
					<Col md={8} mdOffset={2}>
						{content}
					</Col>
				</Row>
			</Grid>
			{ busy ? <div className="overlay"><Icon name={'spinner fa-pulse fa-4x'} /></div> : null }
		</div>
	}
}
export default App;